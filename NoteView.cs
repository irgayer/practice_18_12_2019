﻿using System.Collections.Generic;

namespace Practice_18_12_2019
{
    public class NoteView
    {
        public string Name { get; set; }
        public List<Point> Points { get; set; } = new List<Point>();
        public int Count
        {
            get
            {
                return Points.Count;
            }
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
