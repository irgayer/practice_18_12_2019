﻿using System.Windows;
namespace Practice_18_12_2019
{
    /// <summary>
    /// Interaction logic for AddNoteWindow.xaml
    /// </summary>
    public partial class AddNoteWindow : Window
    {
        public string NoteName { get; set; }
        public AddNoteWindow()
        {
            InitializeComponent();
        }

        private void AddButtonClick(object sender, RoutedEventArgs e)
        {
            if(!string.IsNullOrWhiteSpace(nameBox.Text))
            {
                NoteName = nameBox.Text;
                this.DialogResult = true;

                this.Close();
            }
            else
            {
                MessageBox.Show("Ошибка! Введите имя!");
            }
        }
    }
}
