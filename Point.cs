﻿using System;

namespace Practice_18_12_2019
{
    public class Point
    {
        public Guid Guid { get; set; } = Guid.NewGuid();
        public string PointNote { get; set; } 
        public bool IsChecked { get; set; } = false;
    }
}
