﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace Practice_18_12_2019
{
    public static class FileManager
    {
        public static void WriteNotes(List<NoteView> notes, string fileName)
        {
            using(StreamWriter file = new StreamWriter(fileName))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(file, notes);
            }
        } 
        public static List<NoteView> ReadNotes(string fileName)
        {
            List<NoteView> result = new List<NoteView>();
            
            using (StreamReader file = new StreamReader(fileName))
            {
                JsonSerializer serializer = new JsonSerializer();
                result = (List<NoteView>)serializer.Deserialize(file, typeof(List<NoteView>));
            }

            return result;
        }
    }
}
