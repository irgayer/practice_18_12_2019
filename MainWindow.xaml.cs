﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace Practice_18_12_2019
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const int CHECKBOX_WIDTH = 350;
        const int CHECKBOX_HEIGHT = 40;
        const int CHECKBOX_X_INDENT = 30;
        const int CHECKBOX_Y_INDENT = 0;
        const string FILENAME = "notes.json";

        private List<NoteView> notes;
        private NoteView choosedNote;
        private bool isNoteOrPointChanged;

        public MainWindow()
        {
            InitializeComponent();
            notes = FileManager.ReadNotes(FILENAME);
            choosedNote = null;
            isNoteOrPointChanged = false;

            listView.ItemsSource = notes;
        }

        private void AddNoteButtonClick(object sender, RoutedEventArgs e)
        {
            var w = new AddNoteWindow();
            if (w.ShowDialog() == true)
            {
                string name = w.NoteName;
                notes.Add(new NoteView()
                {
                    Name = name
                });

                isNoteOrPointChanged = true;
            }

            RefreshListView();
        }

        private async void RefreshListView()
        {
            listView.ItemsSource = notes;
            await Task.Run(()=>  Dispatcher.Invoke(() => listView.Items.Refresh()));
        }

        private async void AddPointButtonClick(object sender, RoutedEventArgs e)
        {
            var w = new AddNoteWindow();
            w.Title = "Добавить пункт";
            if (w.ShowDialog() == true)
            {
                string name = w.NoteName;
                choosedNote.Points.Add(new Point
                {
                    PointNote = name
                });

                isNoteOrPointChanged = true;
            }

            await Task.Run(() => RefreshScrollViewer(choosedNote));
        }

        private async void RefreshScrollViewer(NoteView note)
        {
            Dispatcher.Invoke(() => {
                stackPanel.Children.Clear();
                DrawPointsByNote(note);
            });
           

            await Task.Run(() => Dispatcher.Invoke(() =>addPointButton.IsEnabled = true));
        }

        private void ListViewPreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var item = (sender as ListView).SelectedItem;

            if (item != null)
            {
                choosedNote = item as NoteView;
                RefreshScrollViewer(choosedNote);
            }
        }
        private void DrawPointsByNote(NoteView note)
        {
            List<CheckBox> checkBoxes = new List<CheckBox>();
            int i = 1;
            foreach (var point in note.Points)
            {
                checkBoxes.Add(CheckBoxFactory(point.PointNote, point.IsChecked, CHECKBOX_X_INDENT, CHECKBOX_Y_INDENT, $"ch_{point.Guid.ToString().Substring(0, 5)}"));
                i++;
            }

            foreach (var checkBox in checkBoxes)
            {
                stackPanel.Children.Add(checkBox);
            }
        }

        //final
        private CheckBox CheckBoxFactory(string pointNote, bool isChecked, int marginX, int marginY, string name)
        {
            CheckBox result = new CheckBox();

            result.Name = name;
            result.IsChecked = isChecked;
            result.Content = pointNote;
            result.VerticalAlignment = VerticalAlignment.Top;
            result.HorizontalAlignment = HorizontalAlignment.Left;
            result.Margin = new Thickness(marginY, marginX, 0, 0);
            result.Height = CHECKBOX_HEIGHT;
            result.Width = CHECKBOX_WIDTH;
            result.Visibility = Visibility.Visible;
            result.Checked += CheckBoxChecked;
            result.Unchecked += CheckBoxChecked;

            return result;
        }

        private void CheckBoxChecked(object sender, RoutedEventArgs e)
        {
            foreach (var obj in stackPanel.Children)
            {
                if (obj.GetType() == typeof(CheckBox))
                    foreach (var point in choosedNote.Points)
                        if (point.Guid.ToString().Substring(0,5) == (obj as CheckBox).Name.Substring(3))
                            point.IsChecked = (obj as CheckBox).IsChecked == true;              
            }
            isNoteOrPointChanged = true;
        }

        private void WindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (isNoteOrPointChanged == true)
            {
                var dialogResult = MessageBox.Show("Все несохраненные изменения будут потеряны!", "Сохранить?", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (dialogResult == MessageBoxResult.Yes)
                {
                    FileManager.WriteNotes(notes, FILENAME);
                }
            }
        }
    }
}
